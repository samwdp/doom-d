;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-
(toggle-frame-maximized)
(setq-default cursor-type 'hollow)
(set-cursor-color "#ffffff")
;; enabling some modes by default
(display-battery-mode 1)
(global-visual-line-mode)
(mini-frame-mode 1)

(setq user-full-name "Sam Precious"
      user-mail-address "samwdp@gmail.com")
(setq doom-font (font-spec :family "LiterationMono Nerd Font" :size 20)
;; (setq doom-font (font-spec :family "NotoSansMono Nerd Font" :size 17)
      doom-variable-pitch-font (font-spec :family "NotoSans Nerd Font" :size 24))
(setq doom-theme 'gruvbox)
(setq display-line-numbers-type nil)
(setq org-directory "~/org/")
(setq projectile-project-search-path '("W:/"
                                       "W:/personal/angular/src"
                                       "W:/personal/c/src"
                                       "W:/personal/cpp/src"
                                       "W:/personal/csharp/src"
                                       "W:/personal/emacs/src"
                                       "W:/personal/go/src"
                                       "W:/personal/rust/src"
                                       "W:/foresolutions"))

;; custom packages
(use-package! tree-sitter
  :config
  (require 'tree-sitter-langs)
  (global-tree-sitter-mode)
  (add-hook 'tree-sitter-after-on-hook #'tree-sitter-hl-mode))

(after! mini-frame
  (custom-set-variables '(mini-frame-show-parameters
                          '((top . 800)
                            (width . 0.6)
                            (left . 0.5)
                            (height . 25)))))

(use-package! odin-mode
  :hook (odin-mode . rainbow-delimiters-mode)
  :mode "\\.odin\\'"
  :config
  (when (featurep! :tools lsp)
    (defvar lsp-language-id-configuration '((odin-mode . "odin")))
    (lsp-register-client (make-lsp-client :new-connection (lsp-stdio-connection "C:/tools/ols/ols.exe")
                                          :major-modes '(odin-mode)
                                          :server-id 'ols
                                          :multi-root t))
    (add-hook 'odin-mode-hook #'lsp!)))

(after! treemacs
  (setq treemacs-position 'right
        treemacs-width 40))

(when IS-WINDOWS (setq dap-netcore-install-dir "C:/tools/"))
(setq +debugger--dap-alist
  `(((:lang cc +lsp)         :after ccls        :require (dap-lldb dap-gdb-lldb))
    ((:lang elixir +lsp)     :after elixir-mode :require dap-elixir)
    ((:lang go +lsp)         :after go-mode     :require dap-go)
    ((:lang java +lsp)       :after java-mode   :require lsp-java)
    ((:lang csharp +lsp)     :after csharp-mode   :require dap-netcore)
    ((:lang csharp +lsp)     :after csharp-tree-sitter-mode   :require dap-netcore)
    ((:lang php +lsp)        :after php-mode    :require dap-php)
    ((:lang python +lsp)     :after python      :require dap-python)
    ((:lang ruby +lsp)       :after ruby-mode   :require dap-ruby)
    ((:lang rust +lsp)       :after rustic-mode :require (dap-lldb dap-cpptools))
    ((:lang javascript +lsp)
     :after (js2-mode typescript-mode)
     :require (dap-node dap-chrome dap-firefox ,@(if IS-WINDOWS '(dap-edge))))))

;; adding some hooks for auto-mode-list
(add-to-list 'auto-mode-alist '("\\.cshtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.cs\\'" . csharp-tree-sitter-mode))

;; simulating csharp-mode but for treesitter
(defun csharp-treesitter-mode-hooks ()
  (rainbow-delimiters-mode 1)
  (lsp 1))

(add-hook 'csharp-tree-sitter-mode-hook 'csharp-treesitter-mode-hooks)

(defun duplicate-line()
  "Duplicate line that the cursor is on"
  (interactive)
  (move-beginning-of-line 1)
  (kill-line)
  (yank)
  (open-line 1)
  (next-line 1)
  (yank))

(defun toggle-window-dedicated ()
  "Control whether or not Emacs is allowed to display another
buffer in current window."
  (interactive)
  (message
   (if (let (window (get-buffer-window (current-buffer)))
         (set-window-dedicated-p window (not (window-dedicated-p window))))
       "%s: locked"
     "%s: unlocked")
   (current-buffer)))

;; custom keyboard re-mappings
(map!
 :g "M-=" #'toggle-window-dedicated
 :g "C-/" #'comment-line
 :g "C-d" #'duplicate-line
 :n "C-d" #'duplicate-line
 :i "C-d" #'duplicate-line
    :g "C-." #'lsp-execute-code-action
    :n "C-." #'lsp-execute-code-action
    :i "C-." #'lsp-execute-code-action
 :g "C-<" #'evil-window-decrease-width
 :g "C->" #'evil-window-increase-width
 :g "C-h" #'evil-window-left
 :g "C-j" #'evil-window-down
 :g "C-k" #'evil-window-up
 :g "C-l" #'evil-window-right
 :n "g i" #'+lookup/implementations
 :v "g i" #'+lookup/implementations
 :g "C-SPC" #'company-complete)

(after! magit
  (map! (:map magit-mode-map
         :g "C-h" #'evil-window-left
         :g "C-j" #'evil-window-down
         :n "C-j" #'evil-window-down
         :g "C-k" #'evil-window-up
         :n "C-k" #'evil-window-up
         :g "C-l" #'evil-window-right)))

(after! pdf-view-mode
  (map! (:map pdf-view-mode-map
         :g "/" #'pdf-occur
         :i "/" #'pdf-occur
         :n "/" #'pdf-occur)))

(after! eshell
  (map! (:map eshell-mode-map
         :g "C-h" #'evil-window-left
         :g "C-h" #'evil-window-left
         :i "C-h" #'evil-window-left
         :g "C-j" #'evil-window-down
         :n "C-j" #'evil-window-down
         :i "C-j" #'evil-window-down
         :g "C-k" #'evil-window-up
         :n "C-k" #'evil-window-up
         :g "C-l" #'evil-window-right
         :n "C-l" #'evil-window-right
         :i "C-l" #'evil-window-right)
        (:map eshell-prompt-mode-map
         :g "C-h" #'evil-window-left
         :n "C-h" #'evil-window-left
         :g "C-h" #'evil-window-left
         :g "C-j" #'evil-window-down
         :n "C-j" #'evil-window-down
         :i "C-j" #'evil-window-down
         :g "C-k" #'evil-window-up
         :n "C-k" #'evil-window-up
         :i "C-k" #'evil-window-up
         :g "C-l" #'evil-window-right
         :n "C-l" #'evil-window-right
         :i "C-l" #'evil-window-right)))

(after! company
  (setq company-tooltip-limit 200)
  (map! (:map company-active-map
         :g "RET" nil
         :g "<return>" nil
         :g "S-TAB" nil
         :g "S-<tab>" nil
         :g "<tab>" #'company-complete-selection
         :g "TAB" #'company-complete-selection)))

(after! org-brain
  (setq org-brain-include-file-entries t))
  
(after! csharp-mode
  (map! (:map csharp-mode-map
          :localleader
          "d" #'sharper-main-transient)
        (:map csharp-tree-sitter-mode-map
          "d" #'sharper-main-transient)))
